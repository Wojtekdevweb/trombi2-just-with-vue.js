CREATE TABLE IF NOT EXISTS apprenants (
    `list_id` INT,
    `list_Nom` VARCHAR(17) CHARACTER SET utf8,
    `list_Pr_nom` VARCHAR(8) CHARACTER SET utf8,
    `list_Mail` VARCHAR(30) CHARACTER SET utf8,
    `list_Tel` VARCHAR(15) CHARACTER SET utf8,
    `list_Img` VARCHAR(21) CHARACTER SET utf8,
    `list_Naissance` DATETIME
);
INSERT INTO apprenants VALUES
    (1,'Rodrigue','Queraud','queraudrodrigue@outlook.fr','06 70 34 11 12 ','./static/Rodrigue.png','1999-05-19 00:00:00'),
    (2,'Zahir','Massoud','massoudzahir.simplon@gmail.com','06 22 81 55 06','./static/Massoud.png','1993-06-11 00:00:00'),
    (3,'Knapper','Anthony','anthony.knapper@gmail.com','06 50 89 21 91','./static/Anthony.png','1986-06-16 00:00:00'),
    (4,'Bottaro','Gregorio','bottarog95@gmail.com','07 82 64 68 50 ','./static/Gregorio.png','1995-09-16 00:00:00'),
    (5,'Sembel','Aurore','auroresembel@gmail.com','06 80 37 16 13','./static/aurore.png','1988-08-02 00:00:00'),
    (6,'Serres','Florent','serres.florentdu46@gmail.com','06 29 05 83 09','./static/Florent.png','1986-03-26 00:00:00'),
    (7,'Stogren','Wojtek','wojtekdevweb@gmail.com','06 76 46 92 72 ','./static/Wojciech.png','1983-11-24 00:00:00'),
    (8,'Tapin','Jonathan','jonathan.tapin@gmail.com','06 83 84 29 05','./static/Jonathan.png','1991-04-21 00:00:00'),
    (9,'Lison','Lucile','lucilelison@gmail.com','06 74 46 53 41 ','./static/Lucile.png','1989-01-11 00:00:00'),
    (10,'Joffre','Benoit','benoit.joffre911@gmail.com','06 99 21 90 50 ','./static/Benoit.png','1998-08-24 00:00:00'),
    (11,'Pons','Sylvain','sylvainpons680@gmail.com','06 78 29 78 57','./static/Sylvain.png','1996-07-07 00:00:00'),
    (12,'Correa Dos Santos','Bréno','brenocorrea14@gmail.com','07 68 61 08 12','./static/Breno.png','1994-11-16 00:00:00'),
    (13,'Stamierowski','Yannick','yannick.stamierowski@gmail.com','06 73 28 68 38 ','./static/Yannik.png','1981-11-22 00:00:00'),
    (14,'Hama','Rabe','hamarabe46260@gmail.com','06 35 63 17 64','./static/Rabe.png','1980-11-06 00:00:00'),
    (15,'Sayon','Loïc','lsayon@outlook.fr','07 71 68 38 34 ','./static/Loic.png','2000-01-19 00:00:00'),
    (16,'N''Guyen','Julien','juliennguyen1@gmail.com','06 72 56 32 69 ','./static/Julien.png','1977-03-05 00:00:00');
